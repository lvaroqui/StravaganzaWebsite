<?php

/**
 * Created by PhpStorm.
 * User: luc
 * Date: 21/12/2016
 * Time: 16:09
 */
class AdminsController extends AppController
{
    public function isAuthorized($user = null) {
        parent::isAuthorized($user);
        return $user['User']['role'] == 'admin';
    }

    public function Index()
    {

    }

    public function ListOfUsers()
    {
        $this->loadModel('User');
        $users = $this->User->find('all', array(
            'recursive' => -1,
            'order' => 'role ASC'
        ));

        $this->set('users', $users);
    }

    public function ShowDetailsUser($id)
    {
        $this->loadModel('User');
        $user = $this->User->find('first', array(
            'conditions' => array('User.id' => $id)
        ));
        $this->set('user', $user);

    }

    public function ChangeRole($id, $role)
    {
        $this->loadModel('User');
        $user = $this->User->find('first', array(
            'conditions' => array('User.id' => $id)));

        if ($this->User->hasAny(array('User.id' => $id)) && ($role === 'visitor' || $role === 'musician') && $user['User']['role'] != 'admin') {
            $this->User->id = $id;
            $this->User->set(array(
                'role' => $role
            ));
            $this->User->save();
            die('Rôle changé !');
        } else {
            header('500 Internal Server Error', true, 500);
            die('Impossible de changer le rôle');
        }
    }

    public function ListOfInstruments()
    {
        $this->loadModel('InstruCategory');
        $instrucategories = $this->InstruCategory->find('all', array(
            'recursive' => 1,
        ));
        $this->set('instrucategories', $instrucategories);
    }

    public function AddInstruments()
    {
        $this->loadModel('InstruCategory');
        if ($this->request->is('post')) {
            $this->loadModel('Instrument');
            if (!$this->Instrument->findByName($this->request->data['name'])) {
                $this->Instrument->save(array(
                    'name' => $this->request->data['name'],
                    'instrucategory_id' => $this->request->data['category']
                ));
            }

            $this->redirect('/Admins/ListOfInstruments');
        } else {
            $categories = $this->InstruCategory->find('all', array(
                'recursive' => -1
            ));
            $this->set('categories', $categories);
        }
    }

    public function GalleryManagement()
    {
        $this->loadModel('Image');
        $images = $this->Image->find('all', array(
            'conditions' => array('Image.attribute' => 'gallery')
        ));
        $this->set('images', $images);
        //debug($this->request->data);die;
        if (!empty($this->request->data)){
            $extension = strtolower(pathinfo($this->request->data['Image']['image_file']['name'] , PATHINFO_EXTENSION));
            if(
                !empty($this->request->data['Image']['image_file']['tmp_name']) &&
                in_array($extension, array('jpeg', 'jpg', 'png'))
            ){
                $this->Image->save($this->request->data);
                $this->Image->saveField('attribute', 'gallery');
                move_uploaded_file($this->request->data['Image']['image_file']['tmp_name'], IMAGES . 'gallery' . DS . $this->Image->id . '.' . $extension );
                $this->Image->saveField('url', IMAGES . 'gallery' . DS . $this->Image->id . '.' . $extension);
                $this->redirect('/Admins/GalleryManagement');
            }
            else {
                $this->Session->setFlash("Erreur");
            }
        }
    }

    public function DeleteImage($id){
        $this->loadModel('Image');
        if (isset($id) && ($image = $this->Image->find('first', array('conditions' => array('id' => $id)))) != null) {
            if (unlink($image['Image']['url'])) {
                $this->Image->delete($id);
                die('Photo supprimée !');
            } else {
                header('500 Internal Server Error', true, 500);
                die('Impossible de supprimer la photo');
            }
        };
    }
}