<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'Session',
        'Auth' => array(
            'authorize' => array('Controller')
        )
    );

    public function beforeFilter() {
        $this->Auth->loginAction = array('controller' => 'Home', 'action' => 'Index');
        parent::beforeFilter();
        $this->set('authUser', $this->Auth->user('User'));

        setlocale(LC_TIME, 'fr_FR.utf8','fra');
    }

    protected function getSemester() {
        $startSpring = DateTime::createFromFormat('m-d', '02-01');
        $startFall = DateTime::createFromFormat('m-d', '08-01');

        $current_date = DateTime::createFromFormat(date("Y-m-d H:i:s"), date("Y-m-d H:i:s"));

        if ($current_date->format('md') > $startFall->format('md') ||
            $current_date->format('md') < $startSpring->format('md')) {
            return "A" . date("y");
        }
        else {
            return "P" . date("y");
        }
    }

    public function isAuthorized($user = null) {
        return true;
    }


}
