<?php

class ConcertsController extends AppController
    {


    public function isAuthorized($user = null) {
        parent::isAuthorized($user);
        if (in_array($user['User']['role'], array('musician', 'admin'))) {
            return true;
        }
        return false;
    }

        public function Index() {
            $this->loadModel('Concert');
            $concerts = $this->Concert->find('all',array(
                'order' => 'date DESC'
            ));

            $this->set('concerts', $concerts);
        }

        public function ShowDetailsConcert($id) {
            if ($this->request->is('post')) {
                $this->Concert->id = $this->request->data['id'];
                $this->Concert->save(array(
                    'name' => $this->request->data['name'],
                    'place' => $this->request->data['place'],
                    'adress' => nl2br($this->request->data['adress']),
                    'date' => $this->request->data['date'],
                    'details' => nl2br($this->request->data['details'])
                ));
            }
            $concert = $this->Concert->findById($id);
            $this->set('concert', $concert['Concert']);
        }

        public function AddConcert() {
            if ($this->request->is('post')) {
                $this->Concert->save(array(
                    'name' => $this->request->data['name'],
                    'place' => $this->request->data['place'],
                    'adress' => nl2br($this->request->data['adress']),
                    'date' => $this->request->data['date'],
                    'details' => nl2br($this->request->data['details'])
                ));
                $this->redirect('/Concerts/');
            }
        }

    public function DeleteConcert($id) {
        if (isset($id) && ($concert = $this->Concert->findById($id))) {

            if ($this->Concert->delete($id, true)) {
                die('Partition supprimée !');
            } else {
                header('500 Internal Server Error', true, 500);
                die('Impossible de supprimer la partition !');
            }
        };
    }
    }