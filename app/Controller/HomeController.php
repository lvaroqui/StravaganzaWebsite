<?php
/**
 * Created by PhpStorm.
 * Users: luc
 * Date: 01/12/2016
 * Time: 15:03
 */
class HomeController extends AppController {
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    public function Index() {

    }

    public function Concerts() {

    }

    public function Gallery() {
        $this->loadModel('Image');
        $images = $this->Image->find('all', array(
            'conditions' => array('Image.attribute' => 'gallery')
        ));
        $this->set('images', $images);

    }

    public function Historical() {

    }

    public function Contact() {

    }
}