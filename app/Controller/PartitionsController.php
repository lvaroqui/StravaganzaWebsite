<?php
/**
 * Created by PhpStorm.
 * Users: luc
 * Date: 01/12/2016
 * Time: 15:03
 */
class PartitionsController extends AppController {

    public function isAuthorized($user = null) {
        parent::isAuthorized($user);
        if ($user['User']['role'] != 'admin' &&
            in_array($this->action, array('AddPartition', 'AddSong', 'DeletePartition', 'PrintSummary', 'ValidatePrint', 'UnvalidatePrint'))){
            return false;
        }

        if (in_array($user['User']['role'], array('musician', 'admin'))) {
            return true;
        }
        return false;
    }

    public function Index() {
        $this->loadModel('Song');
        $songs = $this->Song->find('all', array(
            'recursive' => -1,
            'conditions' => array('semester' => parent::getSemester()),
            'order' => array('id' => 'DESC')
        ));
        $this->set('songs', $songs);
    }

    public function ListOfPartitions($song_id) {
        $this->loadModel('Song');
        $song = $this->Song->find('first', array('recursive' => 0, 'conditions' => array('id' => $song_id)));
        $parts = $this->Partition->find('all', array(
            'conditions' => array(
                'song_id' => $song_id),
            'recursive' => 2,
            'contain' => array('PartitionUser', 'Song', 'Instrument'),
            'order' => array('Instrument.name ASC', 'Partition.tag ASC')
        ));

        $partitions = array();
        foreach ($parts as $partition) {
            $isRequested = false;
            if (!empty($partition['PartitionUser']['0']) && !(in_array($partition['PartitionUser'][count($partition['PartitionUser'])-1]['tag'], array("cancelled", "validated")))) {
                $isRequested = true;
            }
            if ($partition['Partition']['tag'] == 'conduct') {
                array_push($partitions, array(
                    "name" => "Conducteur - " . $partition['Song']['name'],
                    "id" => $partition['Partition']['id'],
                    "isRequested" => $isRequested)
                );
            }
            else  {
                array_push($partitions, array(
                        "name" => $partition['Instrument']['name'] . ' ' . $partition['Partition']['tag'] . ' - ' . $partition['Song']['name'],
                        "id" => $partition['Partition']['id'],
                        "isRequested" => $isRequested
                    )

                );
            }
        }
        $this->set('song_name', $song['Song']['name']);
        $this->set('partitions', $partitions);
        $this->set('song_id', $song_id);
    }

    public function DownloadPartition($id) {
        // Retrieve the file ready for download
        $partition = $this->Partition->findById($id);
        if (empty($partition)) {
            throw new NotFoundException();
        }
        $name = "";
        if ($partition['Partition']['tag'] == 'conduct') {
            $name = "Conducteur - " . $partition['Song']['name'].'.pdf';
        }
        else  {
            $name = $partition['Instrument']['name'] . ' ' . $partition['Partition']['tag'] . ' - ' . $partition['Song']['name'] .'.pdf';
        }
        $this->response->file(
            $partition['Partition']['url'],
            array(
                'download' => true,
                'name' => $name
            )
        );
        return $this->response;
    }

    public function AskPrint($id) {

        if (!isset($id)) {
            header('500 Internal Server Error', true, 500);
            die("Echec de la requête !");
        }
        $this->loadModel('PartitionUser');
        $claim = $this->PartitionUser->find('first', array(
            'conditions' => array(
                'partition_id' => $id,
                'user_id' => $this->Auth->user('User.id')),
            'order' => array('PartitionUser.id DESC')
        ));
        if (!$claim) {
            if ($this->PartitionUser->save(array(
                'partition_id' => $id,
                'user_id' => $this->Auth->user('User.id')
            ))) {
                die('Requête effectuée ! !');
            } else {
                header('500 Internal Server Error', true, 500);
                die("Echec de la requête !");
            }
        }
        $d1 = new DateTime($claim['PartitionUser']['created']);
        $d2 = new DateTime("now");
        $diff=$d2->diff($d1);
        if ($claim['PartitionUser']['tag'] == null) {
            die('Impression déjà demandée !!');
        }
        else if ($diff->days > 5 || $claim['PartitionUser']['tag'] == "validated") {
            if ($this->PartitionUser->save(array(
                'partition_id' => $id,
                'user_id' => $this->Auth->user('User.id')
            ))) {
                die('Requête effectuée ! !');
            } else {
                header('500 Internal Server Error', true, 500);
                die("Echec de la requête !");
            }
        }
        else if ($claim['PartitionUser']['tag'] == "cancelled") {
            $this->PartitionUser->id = $claim['PartitionUser']['id'];

            if ($this->PartitionUser->saveField('tag', null)) {
                die('Requête effectuée ! !');
            } else {
                header('500 Internal Server Error', true, 500);
                die("Echec de la requête !");
            }
        }
    }

    public function CancelPrint($id) {
        if (!isset($id)) {
            header('500 Internal Server Error', true, 500);
            die("Echec de la requête !");
        }
        $this->loadModel('PartitionUser');
        if (isset($id) && ($claim = $this->PartitionUser->find('first', array(
                'conditions' => array(
                    'partition_id' => $id,
                    'user_id' => $this->Auth->user('User.id')
                ),
                'order' => array('PartitionUser.id DESC')
            )))) {
            $this->PartitionUser->id = $claim['PartitionUser']['id'];
            if ($this->PartitionUser->saveField('tag', "cancelled")) {
                die('Requête effectuée ! !');
            } else {
                header('500 Internal Server Error', true, 500);
                die("Echec de la requête !");
            }
        }
    }

    public function AddSong() {
        if ($this->request->is('post')) {
            $this->loadModel('Song');
            if (!$this->Song->findByName($this->request->data['name'])) {
                $this->Song->save(array(
                    'name' => $this->request->data['name'],
                    'composer' => $this->request->data['composer'],
                    'arranger' => $this->request->data['arranger'],
                    'semester' => parent::getSemester()
                ));
            }

            $this->redirect("/Partitions/");
        }
    }

    private static function wd_remove_accents($str, $charset='utf-8')
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

        return $str;
    }

    public function AddPartition($song_id) {

        if ($this->request->is('post')) {

            $extension = strtolower(pathinfo($this->request->data['Partition']['part_file']['0']['name'] , PATHINFO_EXTENSION));
            if ($this->request->data['instrument_id'] != "auto") {
                if (
                    !empty($this->request->data['Partition']['part_file'][0]['tmp_name']) &&
                    in_array($extension, array('pdf'))
                ) {
                    if ($this->request->data['instrument_id'] == 'conduct' &&
                        !$this->Partition->find('first', array(
                            'conditions' => array('Song.id' => $this->request->data['song_id'], 'tag' => 'conduct')))
                    ) {

                        $this->Partition->save(array(
                            'instrument_id' => null,
                            'song_id' => $this->request->data['song_id'],
                            'tag' => 'conduct'
                        ));
                    } else if (!$this->Partition->find('first', array(
                        'conditions' => array('Song.id' => $this->request->data['song_id'], 'instrument_id' => $this->request->data['instrument_id'], 'tag' => $this->request->data['tag'])))
                    ) {
                        $tag = $this->request->data['tag'];
                        if ($tag == 'null') {
                            $tag = "";
                        }
                        $this->Partition->save(array(
                            'instrument_id' => $this->request->data['instrument_id'],
                            'song_id' => $this->request->data['song_id'],
                            'tag' => $tag
                        ));
                    } else {
                        $this->redirect("/Partitions/ListOfPartitions/" . $song_id);
                    }

                    $path = WWW_ROOT . 'files' . DS . 'partitions' . DS . $this->Partition->id . '.' . $extension;
                    move_uploaded_file($this->request->data['Partition']['part_file'][0]['tmp_name'], $path);
                    $this->Partition->saveField('url', $path);
                }
            }
            else {
                $this->loadModel("Instrument");
                $instruments = $this->Instrument->find('all', array('recursive' => 0, 'order' => array('Instrument.name ASC')));
                usort($instruments, function($a,$b) use ($instruments) {
                            return strlen($b['Instrument']['name'])-strlen($a['Instrument']['name']);
                        });
                foreach ($this->request->data['Partition']['part_file'] as $part) {

                    foreach ($instruments as $instrument) {
                        $subject = PartitionsController::wd_remove_accents(str_replace(' ', '_', $part['name']));
                        $search = PartitionsController::wd_remove_accents(str_replace(' ', '_', $instrument['Instrument']['name']));
                        $subject = strtolower(str_replace(' ', '_', $subject));
                        $search = strtolower(str_replace(' ', '_', $search));


                        if (strpos($subject, $search) !== false) {
                            $tag = "";

                            for ($i = 1; $i <= 4; $i++) {
                                if (strpos($subject, strval($i)) !== false) {
                                    $tag = $i;
                                }
                            }
                            $this->Partition->create();
                            $this->Partition->save(array(
                                'instrument_id' => $instrument['Instrument']['id'],
                                'song_id' => $this->request->data['song_id'],
                                'tag' => $tag
                            ));
                            $path = WWW_ROOT . 'files' . DS . 'partitions' . DS . $this->Partition->id . '.' . $extension;

                            move_uploaded_file($part['tmp_name'], $path);
                            $this->Partition->saveField('url', $path);
                            break;
                        }
                    }
                }
            }
            $this->redirect("/Partitions/ListOfPartitions/" . $song_id);
        }
        else {
            $this->loadModel('Instrument');
            $instruments = $this->Instrument->find('all', array(
                'order' => array('Instrument.name ASC')
            ));
            $this->loadModel('Song');
            $song = $this->Song->find('first', array(
                'conditions' => array(
                    'id' => $song_id
                )
            ));
            $this->set('song_name', $song['Song']['name']);
            $this->set('song_id', $song_id);
            $this->set('instruments', $instruments);
        }

    }




    public function DeletePartition($id) {
        if (isset($id) && ($partition = $this->Partition->findById($id))) {

            if (unlink($partition['Partition']['url'])) {
                $this->Partition->delete($id, true);
                die('Partition supprimée !');
            } else {
                header('500 Internal Server Error', true, 500);
                die('Impossible de supprimer la partition !');
            }
        };
    }

    public function PrintSummary() {
        $this->loadModel("PartitionUser");
        $claims = $this->PartitionUser->find("all", array(
            "contain" => array("Partition", "User", "Partition.Instrument", "Partition.Song"),
        ));

        $claimsBySong = array();
        $validatedClaims = array();
        foreach ($claims as $claim) {
            $name = "";
            if ($claim['Partition']['tag'] == "conduct") {
                $name = "Conducteur";
            }
            else {
                $name = $claim['Partition']['Instrument']['name'] . " " . $claim['Partition']['tag'];
            }
            if ($claim['PartitionUser']['tag'] == null) {
                if (empty($claimsBySong[$claim['Partition']['Song']['name']])) {
                    $claimsBySong[$claim['Partition']['Song']['name']] = array();
                }

                array_push($claimsBySong[$claim['Partition']['Song']['name']], array(
                    "song_name" => $claim['Partition']['Song']['name'],
                    "name" => $name,
                    "idPart" => $claim['Partition']['id'],
                    "idClaim" => $claim['PartitionUser']['id'],
                    "idUser" => $claim['User']['id'],
                    "askedBy" => $claim['User']['first_name'] . " " . $claim['User']['last_name'],
                ));
            }

            elseif ($claim['PartitionUser']['tag'] == "validated") {
                array_push($validatedClaims, array(
                    "name" => $name,
                    "song_name" => $claim['Partition']['Song']['name'],
                    "idPart" => $claim['Partition']['id'],
                    "idClaim" => $claim['PartitionUser']['id'],
                    "idUser" => $claim['User']['id'],
                    "askedBy" => $claim['User']['first_name'] . " " . $claim['User']['last_name'],
                ));
            }


        };
        $this->set('songs', $claimsBySong);
        $this->set('validatedClaims', $validatedClaims);
    }

    public function ValidatePrint($id) {
        $this->loadModel("PartitionUser");
        $this->PartitionUser->id = $id;
        $this->PartitionUser->saveField('tag', "validated");
        $this->redirect("/Partitions/PrintSummary");
    }

    public function UnvalidatePrint($id) {
        $this->loadModel("PartitionUser");
        $this->PartitionUser->id = $id;
        $this->PartitionUser->saveField('tag', null);
        $this->redirect("/Partitions/PrintSummary");
    }
}