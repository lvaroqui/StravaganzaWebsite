<?php
/**
 * Created by PhpStorm.
 * User: luc
 * Date: 02/12/2016
 * Time: 10:07
 */
use XmlToArrayParser\XmlToArrayParser;


class UsersController extends AppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('Login', 'Register');
        //$this->Auth->allow('FakeLogin');
        $this->Auth->deny('FakeLogin');
    }

    public function isAuthorized($user = null) {
        parent::isAuthorized($user);

        return true;
    }

    public function Login()
    {
        require_once(ROOT . DS . "vendors" . DS . "XmlToArrayParser" . DS . "XmlToArrayParser.php");

        $ticket = $this->request->query['ticket'];
        $service = 'http://assos.utc.fr/stravaganza/Users/Login';

        $url_validate = "https://cas.utc.fr/cas/serviceValidate?service=" . $service . "&ticket=" . $ticket;
        $data = file_get_contents($url_validate);

        if (empty($data)) {
            throw new NotFoundException(__("Impossible de se connecter au CAS. Impossible de valider le ticket."));
        }


        $parsed = new XmlToArrayParser($data);

        if (!isset($parsed->array['cas:serviceResponse']['cas:authenticationSuccess']['cas:user'])) {
            if (isset($parsed->array['cas:serviceResponse']['cas:authenticationFailure']['cdata'])) {
                throw new NotFoundException(__("Impossible de se connecter au CAS : " . $parsed->array['cas:serviceResponse']['cas:authenticationFailure']['cdata']));
            } else {
                throw new NotFoundException(__("Impossible de se connecter au CAS : réponse illisible."));
            }
        }
        $username = strtolower($parsed->array['cas:serviceResponse']['cas:authenticationSuccess']['cas:user']);

        if (!isset($parsed->array['cas:serviceResponse']['cas:authenticationSuccess']['cas:attributes']['cas:mail'])) {
            throw new NotFoundException(__("Impossible de se connecter au CAS. Impossible de lire l'adresse email."));
        }
        $email = $parsed->array['cas:serviceResponse']['cas:authenticationSuccess']['cas:attributes']['cas:mail'];
        $lastname = $parsed->array['cas:serviceResponse']['cas:authenticationSuccess']['cas:attributes']['cas:sn'];
        $firstname = $parsed->array['cas:serviceResponse']['cas:authenticationSuccess']['cas:attributes']['cas:givenName'];

        $user = $this->User->find('first', array(
            'conditions' => array('User.username' => $username)));
        if (!$this->User->findByUsername($username)) {

            $this->Session->write('userInfosCAS', array(
                'username' => $username,
                'email' => $email,
                'lastname' => $lastname,
                'firstname' => $firstname
            ));
            $this->redirect('/Users/Register');
        } else {
            $this->Auth->login($user);
            $this->redirect('/Users/Profile');
        }

    }

    public function FakeLogin($username)
    {
        $user = $this->User->find('first', array(
            'conditions' => array('User.username' => $username)));

        $this->Auth->login($user);
        $this->redirect('/Users/Profile');
    }

    public function Logout()
    {
        $this->Auth->logout();
        $this->redirect('https://cas.utc.fr/cas/logout?url=http://assos.utc.fr/stravaganza/');
    }

    public function Register()
    {
        if (true or !is_null($this->Session->read('userInfosCAS'))) {
            if ($this->request->is('post')) {
                $userInfosCAS = $this->Session->read('userInfosCAS');

                $username = $userInfosCAS['username'];
                $firstname = $userInfosCAS['firstname'];
                $lastname = $userInfosCAS['lastname'];
                $email = $userInfosCAS['email'];

                $this->User->save(array(
                    'username' => $username,
                    'first_name' => $firstname,
                    'last_name' => $lastname,
                    'email' => $email,
                    'role' => "musician"
                ));

                $user = $this->User->find('first', array(
                    'conditions' => array('User.username' => $username)));
                $this->Auth->login($user);
                $this->redirect('/Users/ChooseInstruments');
            } else {

                $this->Session->read('userInfosCAS');

                $userInfosCAS = $this->Session->read('userInfosCAS');

                $this->set('username', $userInfosCAS['username']);
                $this->set('email', $userInfosCAS['email']);
                $this->set('lastname', $userInfosCAS['lastname']);
                $this->set('firstname', $userInfosCAS['firstname']);
            }
        } else {
            $this->redirect('/Home/Index');
        }
    }

    public function Profile()
    {
        $this->loadModel("InstrumentUser");
        $this->loadModel("Image");
        $profile_picture = $this->Image->find('first', array(
            'conditions' => array('id' =>  $this->Auth->user('User.image_id'))));
        $this->set('profile_picture', $profile_picture);
        $this->InstrumentUser->contain('Instrument');
        $currentMainInstrument = $this->InstrumentUser->find('first', array(
            'conditions' => array('InstrumentUser.user_id' => $this->Auth->user('User.id'), 'InstrumentUser.isMain' => true),
            'fields' => array('Instrument.name')
        ));
        $currentInstruments = $this->InstrumentUser->find('all', array(
            'conditions' => array('InstrumentUser.user_id' => $this->Auth->user('User.id')),
            'fields' => array('Instrument.name')
        ));

        if ((empty($currentMainInstrument))) {
            $currentInstruments = array(array('Instrument' => array('name' => 'Aucun instrument renseigné')));
        } else {
            $this->set('currentMainInstrument', $currentMainInstrument['Instrument']['name']);
        }
        $this->set('currentInstruments', $currentInstruments);
    }

    public function ChooseInstruments()
    {
        $this->loadModel("InstrumentUser");
        if ($this->request->is('post')) {
            if (!is_null($this->request->data['User']['Instrument'])) {

                $this->InstrumentUser->deleteAll(array('user_id' => $this->Auth->user('User.id')));
                $first = true;
                foreach ($this->request->data['User']['Instrument'] as $instruId) {
                    if ($first == true) {
                        $this->InstrumentUser->create();
                        $this->InstrumentUser->save(array(
                            'user_id' => $this->Auth->user('User.id'),
                            'instrument_id' => $instruId,
                            'isMain' => 1
                        ));
                        $first = false;
                    } else {
                        $this->InstrumentUser->create();
                        $this->InstrumentUser->save(array(
                            'user_id' => $this->Auth->user('User.id'),
                            'instrument_id' => $instruId
                        ));
                    }
                }
                $this->redirect('/Users/ChooseFavoriteInstrument');
            } else {
                $this->redirect('/Users/Profile');
            }

        } else {
            $categories = $this->User->Instrument->InstruCategory->find('all');
            $instruUserDB = $this->InstrumentUser->find('all', array(
                'conditions' => array('InstrumentUser.user_id' => $this->Auth->user('User.id')),
                'fields' => array('Instrument.id')
            ));

            $instruUser = array();

            foreach ($instruUserDB as $instru) {
                array_push($instruUser, $instru['Instrument']['id']);
            }

            $this->set('instruUser', $instruUser);
            $this->set('categories', $categories);
        }
    }

    public function ChooseFavoriteInstrument()
    {
        $this->loadModel('InstrumentUser');
        $this->InstrumentUser->contain('Instrument');
        if ($this->request->is('post')) {
            $this->InstrumentUser->updateAll(
                array('InstrumentUser.isMain' => 0),
                array('InstrumentUser.user_id' => $this->Auth->user('User.id'))
            );
            debug($this->request->data);
            $toModify = $this->InstrumentUser->find('first', array(
                    'conditions' => array(
                        'InstrumentUser.user_id' => $this->Auth->user('User.id'),
                        'InstrumentUser.instrument_id' => $this->request->data['main_instrument']
                    ),
                    'fields' => array('InstrumentUser.id'))
            );

            $this->InstrumentUser->id = $toModify['InstrumentUser']['id'];
            $this->InstrumentUser->save(array('id' => $toModify['InstrumentUser']['id'], 'isMain' => 1));

            $this->redirect('/Users/Profile');
        } else {
            $instruments = $this->InstrumentUser->find('all', array(
                'conditions' => array('InstrumentUser.user_id' => $this->Auth->user('User.id')),
                'fields' => array('InstrumentUser.isMain', 'Instrument.id', 'Instrument.name'),
                'recursive' => 0
            ));
            if (empty($instruments)) {
                $this->redirect('/Users/Profile');
            }
            $this->set('instruments', $instruments);
        }
    }

    public function UploadProfilePicture() {
        $this->loadModel('Image');
        if (!empty($this->request->data)){
            $extension = strtolower(pathinfo($this->request->data['Image']['image_file']['name'] , PATHINFO_EXTENSION));
            if(
                !empty($this->request->data['Image']['image_file']['tmp_name']) &&
                in_array($extension, array('jpeg', 'jpg', 'png'))
            ){
                $this->Image->save($this->request->data);
                $this->Image->saveField('attribute', 'trombi');
                move_uploaded_file($this->request->data['Image']['image_file']['tmp_name'], IMAGES . 'gallery' . DS . $this->Image->id . '.' . $extension );
                $this->Image->saveField('url', IMAGES . 'gallery' . DS . $this->Image->id . '.' . $extension);
                $currentUser = $this->Auth->user('User');
                $this->User->id = $currentUser['id'];
                $this->User->saveField('image_id', $this->Image->id);
                $this->redirect('/Users/Profile');
            }
            else {
                $this->Session->setFlash("Erreur");
            }
        }
    }
}