<?php
class Instrument extends AppModel
{
    public $useTable = 'instruments';
    public $belongsTo = array('InstruCategory'=> array(
        'foreignKey' => 'instrucategory_id'));
    public $hasMany = array('Partition');
    public $hasAndBelongsToMany = array('User');
}