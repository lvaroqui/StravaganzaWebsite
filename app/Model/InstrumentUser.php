<?php

/**
 * Created by PhpStorm.
 * Users: luc
 * Date: 02/12/2016
 * Time: 10:52
 */
class InstrumentUser extends AppModel
{
    public $useTable = "instruments_users";
    public $actsAs = array('containable');
    public $belongsTo = array('Instrument', 'User');
}