<?php

class Partition extends AppModel
{
    public $useTable = 'partitions';
    public $belongsTo = array(
        'Song'=> array('foreignKey' => 'song_id'),
        'Instrument' => array('foreignKey' => 'instrument_id')
    );
    public $hasAndBelongsToMany = array('User');
    public $hasMany = array('PartitionUser');
    public $actsAs = array('containable');
}