<?php
    class PartitionUser extends AppModel
    {
        public $useTable = "partitions_users";
        public $actsAs = array('containable');
        public $belongsTo = array('Partition', 'User');
    }