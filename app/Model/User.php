<?php

/**
 * Created by PhpStorm.
 * Users: luc
 * Date: 02/12/2016
 * Time: 10:52
 */
class User extends AppModel
{
    public $hasAndBelongsToMany = array('Instrument', 'Partition');
    public $actsAs = array('containable');
}