<div>
    <h1 class="text-center">Ajouter un instrument</h1>
    <p><?= $this->Html->link('Retour', '/Admins/ListOfInstruments/', array('class' => 'btn btn-primary'))?></p>
    <?= $this->Form->create('Instruments', array('class' => "form-inline"));?>
    <div class="form-group">
        <label for="name">Nom de l'instrument :</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>
    <div class="form-group">
        <label for="category">Categorie :</label>
        <select class="form-control" id="category" name="category">
            <?php foreach($categories as $category): ?>
                <option value="<?= $category['InstruCategory']['id']?>"><?=$category['InstruCategory']['name']?></option>
            <?php endforeach; ?>
        </select>
    </div><br/><br/>
    <div>
        <?= $this->Form->End(array('label' => 'Valider', 'class' => 'btn btn-default')) ?>
    </div>
</div>
