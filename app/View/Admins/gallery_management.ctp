<h1 class="text-center">Gestion de la galerie</h1>
<p><?= $this->Html->link('Retour', '/Admins/Index/', array('class' => 'btn grey'))?></p>
<h2>Ajouter une image</h2>

<?= $this->Form->create('Image', array('type' => 'file'));?>

<div class="input-field">
    <textarea id="data[Image][description]" name="data[Image][description]" class="materialize-textarea"></textarea>
    <label for="data[Image][description]">Description</label>
</div>

<div class="file-field input-field">
    <div class="btn grey waves-effect waves-light">
        <span>Photo</span>
        <input type="file" name="data[Image][image_file]">
    </div>
    <div class="file-path-wrapper">
        <input class="file-path validate" type="text" placeholder="La photo (format .jpg ou .png)">
    </div>
</div>

<?= $this->Form->end(array('label' => "Ajouter l'image", 'class' => 'btn orange waves-effect waves-light'));?>
<h2>Images de la gallerie</h2>
<table class="table table-responsive">
    <thead>
    <tr>
        <th/>
        <th>Description</th>
        <th>Date d'ajout</th>
        <th></th>
    </tr>
    </thead>
    <?php foreach ($images as $image): ?>
        <tbody class="white-text">
        <td><img style="max-height: 100px;max-width: 100px;" src="<?= $this->webroot . 'img/gallery/' . pathinfo($image['Image']['url'], PATHINFO_BASENAME)?>"/></td>
        <td><?= $image['Image']['description'] ?></td>
        <td><?= $image['Image']['created'] ?></td>
        <td class="text-center"><div class="btn red deletePhotoGallery" id="<?= $image['Image']['id']?>">Supprimer</div></td>
        </tbody>
    <?php endforeach; ?>
</table>
