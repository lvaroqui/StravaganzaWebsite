<h1 class="text-center">Page d'administration</h1>
<p><?= $this->Html->link('Retour', '/Users/Profile', array('class' => 'waves-effect waves-light btn grey'))?></p>

<div class="row">
    <div class="col s12 m6">

        <h3 class="text-center">Gestion de l'orchestre</h3>
        <p><?= $this->Html->link('Gestion des concerts', '/Concerts/', array('class' => 'waves-effect waves-light btn light-blue full-width'))?></p>
        <p><?= $this->Html->link('Gestion des partitions', '/Partitions/', array('class' => 'waves-effect waves-light btn light-blue full-width'))?></p>
        <p><?= $this->Html->link('Liste des utilisateurs', '/Admins/ListOfUsers', array('class' => 'waves-effect waves-light btn light-blue full-width'))?></p>
    </div>
    <div class="col s12 m6">
        <h3 class="text-center">Autres</h3>
        <p><?= $this->Html->link('Gestion de la galerie', '/Admins/GalleryManagement', array('class' => 'waves-effect waves-light btn light-blue full-width'))?></p>
        <p><?= $this->Html->link('Liste des instruments', '/Admins/ListOfInstruments', array('class' => 'waves-effect waves-light btn light-blue full-width'))?></p>
    </div>
</div>