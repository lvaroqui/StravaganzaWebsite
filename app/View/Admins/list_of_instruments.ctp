<h1 class="text-center">Liste des instruments</h1>
<p><?= $this->Html->link('Retour', '/Admins/Index/', array('class' => 'btn btn-primary'))?></p>
<p><?= $this->Html->link('Ajouter un instrument', '/Admins/AddInstruments/', array('class' => 'btn btn-primary'))?></p>
<?php foreach($instrucategories as $instrucategory):?>
    <h3><?= $instrucategory['InstruCategory']['name']?></h3>
    <table class="table table-responsive">
        <?php foreach($instrucategory['Instrument'] as $instrument):?>
            <tbody class="white-text">
            <td><?= $instrument['name']?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tbody>
        <?php endforeach;?>

    </table>
<?php endforeach;?>
<div id="snackbar"></div>