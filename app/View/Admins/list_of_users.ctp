<h1 class="text-center">Liste des utilisateurs</h1>
<?= $this->Html->link('Retour', '/Admins/Index/', array('class' => 'btn btn-primary')) ?>
<table class="table table-responsive">
    <thead>
    <tr>
        <th></th>
        <th>Prénom</th>
        <th>Nom</th>
        <th>Email</th>
        <th>Rôle</th>
    </tr>
    </thead>

    <?php foreach ($users as $user): ?>
        <tbody class="white-text">
        <td class="text-center"><?= $this->Html->link('Détails', '/Admins/ShowDetailsUser/' . $user['User']['id'], array('class' => 'btn btn-info btn-sm')) ?></td>
        <td><?= $user['User']['first_name'] ?></td>
        <td><?= $user['User']['last_name'] ?></td>
        <td><a href="mailto:<?= $user['User']['email'] ?>"><?= $user['User']['email'] ?></a></td>
        <td>
            <?php if ($user['User']['role'] === 'admin'): ?>
                <p class="bold">Admin</p>
            <?php else: ?>
                <select class="form-control roleSelect">
                    <?php $roles = array('musician', 'visitor');
                    foreach ($roles as $role):
                        if ($role === $user['User']['role']) {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        } ?>
                        <option value="<?= $user['User']['id'] . '/' . $role ?>" <?= $selected ?>><?= ucfirst($role) ?></option>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>


        </td>
        </tbody>
    <?php endforeach; ?>

</table>