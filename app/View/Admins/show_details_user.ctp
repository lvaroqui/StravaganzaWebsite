<h1 class="text-center">Infos de <?= $user['User']['first_name'] . ' ' . $user['User']['last_name']?></h1>

<?= $this->Html->link('Retour', '/Admins/ListOfUsers/', array('class' => 'btn btn-primary'))?>

<h3>Informations personelles</h3>
<ul class="white-text">
    <li>ID Utilisateur : <?= $user['User']['id']?></li>
    <li>Username : <?= $user['User']['username']?></li>
    <li>Nom complet : <?= $user['User']['first_name'] . ' ' . $user['User']['last_name']?></li>
    <li>Email : <a href="mailto:<?= $user['User']['email']?>"><?= $user['User']['email']?></a></li>
    <li>Inscription le : <?= strftime('%d %B %Y', $user['User']['created'])?></li>
</ul><br/>

<h3>Instrument(s) joué(s)</h3>
<ul class="white-text">
    <?php foreach ($user['Instrument'] as $instrument):?>
        <li><?=$instrument['name']?>
            <?php if($instrument['InstrumentsUser']['isMain'] === true){echo '<strong style="margin-left: 30px">Instrument principal</strong>';}?>
        </li>
    <?php endforeach;?>
</ul>