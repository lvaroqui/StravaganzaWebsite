<h1 class="text-center">Ajouter un concert</h1>

<?= $this->Html->link('Retour', '/Concerts/', array('class' => 'btn btn-warning'))?>
<br/><br/>
<?= $this->Form->create();?>
<input type="hidden" name="id">
<div class="form-group">
    <label for="name">Nom du concert</label>
    <input type="text" class="form-control" id="name" name="name">
</div>
<div class="form-group">
    <label for="place">Endroit</label>
    <input type="text" class="form-control" id="place" name="place">
</div>
<div class="form-group">
    <label for="adress">Adresse</label>
    <textarea class="form-control" id="adress" name="adress"></textarea>
</div>
<div class="form-group">
    <label for="date" class="col-2 col-form-label">Date</label>
    <input class="form-control" type="datetime-local" id="date" name="date">
</div>
<div class="form-group">
    <label for="details">Détails</label>
    <textarea class="form-control" id="details" name="details"></textarea>
</div>

<?= $this->Form->end(array('label' => 'Valider', 'class' => 'btn btn-default'));?>
