<h1 class="text-center">Liste des concerts</h1>
<p><?= $this->Html->link('Retour', '/Admins/Index/', array('class' => 'btn btn-warning'))?>
<?php if($authUser['role'] === 'admin'):?>
   <?= $this->Html->link('Ajouter un concert', '/Concerts/AddConcert/', array('class' => 'btn btn-danger pull-right'))?>
<?php endif;?>
</p>


<table class="table table-responsive">
    <thead>
    <tr>
        <th></th>
        <th>Intitulé</th>
        <th>Endroit</th>
        <th>Adresse</th>
        <th>Date</th>
        <th>Détails</th>
        <th></th>
    </tr>
    </thead>

    <?php foreach($concerts as $concert):?>

        <tbody class="white-text">
        <?php if ($authUser['role'] === 'admin') {?>
            <td class="text-center"><?= $this->Html->link('Gérer', '/Concerts/ShowDetailsConcert/' . $concert['Concert']['id'], array('class' => 'btn btn-info btn-sm'))?></td>
        <?php
        } else {
            echo '<td></td>';
        }?>
        <td><?= $concert['Concert']['name']?></td>
        <td><?= $concert['Concert']['place']?></td>
        <td><?= $concert['Concert']['adress']?></td>
        <td><?= date('l d F Y - H:i', strtotime($concert['Concert']['date']))?></td>
        <td><?= $concert['Concert']['details']?></td>
        <td>
            <div class="btn btn-danger pull-right deleteConcert" id="<?= $concert['Concert']['id']?>">
                <span class="glyphicon glyphicon-trash"></span>
            </div>
        </td>
        </tbody>
    <?php endforeach;?>

</table>
<div id="snackbar"></div>