<h1 class="text-center">Infos du concert : <?= $concert['name']?></h1>

<?= $this->Html->link('Retour', '/Concerts/', array('class' => 'btn btn-warning'))?>
<br/><br/>
<?= $this->Form->create();?>
<input type="hidden" name="id" value="<?= $concert['id']?>">
<div class="form-group">
    <label for="name">Nom du concert</label>
    <input type="text" class="form-control" id="name" name="name" value="<?= $concert['name']?>">
</div>
<div class="form-group">
    <label for="place">Endroit</label>
    <input type="text" class="form-control" id="place" name="place" value="<?= $concert['place']?>">
</div>
<div class="form-group">
    <label for="adress">Adresse</label>
    <textarea class="form-control" id="adress" name="adress"><?= str_replace('<br />','',$concert['adress'])?></textarea>
</div>
<div class="form-group">
    <label for="date" class="col-2 col-form-label">Date</label>
    <input class="form-control" type="datetime-local" value="<?= date('Y-m-d',strtotime($concert['date'])) . 'T' . date('H:i:s',strtotime($concert['date']))?>" id="date" name="date">
</div>
<div class="form-group">
    <label for="details">Détails</label>
    <textarea class="form-control" id="details" name="details"><?= str_replace('<br />','',$concert['details'])?></textarea>
</div>

<?= $this->Form->end(array('label' => 'Valider', 'class' => 'btn btn-default'));?>