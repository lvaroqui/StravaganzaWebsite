<?php if(!isset($authUser)){?>
    <?= $this->Html->link('CAS', 'https://cas.utc.fr/cas/login?service=http://assos.utc.fr/stravaganza/Users/Login', array('id' => 'btn-cas', 'class' => 'yellow darken-1 waves-effect waves-light btn')); ?>
<?php
}
else{
    ?>

    <li>
        <?= $this->html->link("Espace Personnel", '/Users/Profile', array('class' => 'waves-effect waves-light btn light-blue'))?>
    </li>
    <li>
        <?= $this->html->link('<i class="material-icons">power_settings_new</i>', '/Users/logout', array('escape'=>false, 'class' => 'hide-on-med-and-down waves-effect waves-light btn-floating orange'))?>
        <?= $this->html->link('Se déconnecter', '/Users/logout', array('escape'=>false, 'class' => 'hide-on-large-only waves-effect waves-light btn  red'))?>
    </li>
<?php
}?>