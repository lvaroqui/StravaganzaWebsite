<?php if(!isset($authUser)){?>
    <?= $this->Html->link('CAS', 'https://cas.utc.fr/cas/login?service=http://assos.utc.fr/stravaganza/Users/Login', array('id' => 'btn-cas', 'class' => 'yellow darken-1 waves-effect waves-light btn')); ?>
<?php
}
else{
    ?>
    <li class="blue lighten-4"><?= $this->html->link('Espace Personnel <i class="material-icons left">account_box</i>', '/Users/Profile', array('escape' => false))?></li>
    <li class="red lighten-4"><?= $this->html->link('Se déconnecté <i class="material-icons left">clear</i>', '/Users/logout', array('escape' => false))?></li>
<?php
}?>