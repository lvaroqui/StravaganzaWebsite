<nav>
    <div class="nav-wrapper orange">
        <?= $this->Html->link('Stravaganza', array('controller' => 'Home', 'action' => 'Index'), array('class' => 'brand-logo', 'style' => 'padding-left:10px'));?>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <?php
            $name = array('Accueil', 'Concerts', 'Galerie', 'Contact');
            $actions = array('Index', 'Concerts', 'Gallery', 'Contact');
            for ($i = 0; $i < count($actions); $i++) {
                if (strtolower($this->params['controller']) == 'home' && strtolower($this->params['action']) == strtolower($actions[$i])) {
                    echo '<li class="active">';
                }
                else {
                    echo '<li>';
                }
                echo $this->Html->link($name[$i], array('controller' => 'Home', 'action' => $actions[$i]));
                echo '</li>';
            }
            ?>
            <?= $this->Element("login")?>
        </ul>
    </div>
</nav>
<ul class="sidenav" id="mobile-demo">
    <?php
    $name = array('Accueil', 'Concerts', 'Galerie', 'Contact');
    $actions = array('Index', 'Concerts', 'Gallery', 'Contact');
    for ($i = 0; $i < count($actions); $i++) {
        if (strtolower($this->params['controller']) == 'home' && strtolower($this->params['action']) == strtolower($actions[$i])) {
            echo '<li class="active">';
        }
        else {
            echo '<li>';
        }
        echo $this->Html->link($name[$i], array('controller' => 'Home', 'action' => $actions[$i]));
        echo '</li>';
    }
    ?>
    <?= $this->Element("loginMobile")?>
</ul>