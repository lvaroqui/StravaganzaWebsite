<?php $this->assign('title', 'Concerts'); ?>

<div class="container" style="margin-bottom: 150px">
    <h5 class="center white-text">Dernier concert de Strava - Concert de Printemps 2018</h5>
    <center>
        <div class="embed-responsive embed-responsive-16by9" style="border:solid; border-color:#3165a4; border-width:4px; background-color: black;">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/i8bcY_SycKo"></iframe>
        </div>
    </center>
    <br />
    <h5 class="center white-text">Festupic 2016</h5>
    <center>
        <div class="embed-responsive embed-responsive-16by9" style="border:solid; border-color:#3165a4; border-width:4px; background-color: black;">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/q5cMRyoebKY"></iframe>
        </div>
    </center>
</div>