<?php $this->assign('title', 'Contact'); ?>

<div class="container center">
    <h3>Contact</h3>
    <p>
        Association Stravaganza
        <br />
        Université de Technologie de Compiègne
        <br />
        Maison des étudiants, centre Benjamin Franklin
        <br />
        BP 60319 - rue Roger Couttolenc - 60203 Compiègne Cedex
        <br />
        E-mail :
        <a href="mailto:stravaganza@assos.utc.fr">stravaganza@assos.utc.fr</a>
    </p>
</div>
