<?php
$this->assign('title', 'Galerie');
$this->Html->css('/unitegallery/themes/default/ug-theme-default', array('inline' => false));
$this->Html->css('/unitegallery/css/unite-gallery', array('inline' => false));
$this->Html->script('/unitegallery/js/unitegallery', array('inline' => false));
$this->Html->script('/unitegallery/themes/default/ug-theme-default', array('inline' => false));
$this->Html->script('/unitegallery/themes/tiles/ug-theme-tiles.js', array('inline' => false));
$this->Html->script('slider_gallery', array('inline' => false));
?>


<div class="container" id="gallery" style="display:none; padding: 60px 50px 0px 50px">
<?php foreach($images as $image):?>
    <img alt="<?= $image['Image']['description']?>" src="<?= $this->webroot . 'img/gallery/' . pathinfo($image['Image']['url'], PATHINFO_BASENAME)?>"
         data-image="<?=$this->webroot . 'img/gallery/' . pathinfo($image['Image']['url'], PATHINFO_BASENAME)?>"
         data-description="<?= $image['Image']['description']?>">
<?php endforeach;?>
</div>
