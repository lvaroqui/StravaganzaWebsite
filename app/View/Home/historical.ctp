<?php $this->assign('title', 'Historical'); ?>

<div class="white-text container">
    <h1 class="text-center">Historique</h1>
    <h3>Répertoire A16</h3>
    <ul>
        <li>Come What May - Moulin Rouge</li>
        <li>Seconde Valse - Chotsakovitch</li>
        <li>Suite Espagnol 1.Allegro - Edouard Lalo</li>
        <li>Galop Infernal - Offenbach</li>
    </ul><br />
    <h3>Répertoire P16</h3>
    <ul>
        <li>Le Château Dans Le Ciel - Joe Hisaichi</li>
        <li>Habanera - Edouard Lalo</li>
        <li>Bagatelle - Antonin Dvorak</li>
        <li>Phantom of the Opera - Andrew Llyod Webber</li>
        <li>Galop Infernal - Offenbach</li>
        <li>Somebody To Love - Freddie Mercury</li>
    </ul>
</div>