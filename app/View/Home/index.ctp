<?php
    $this->assign('title', 'Accueil');
?>

<br/>

<h3 class="text-center">Bienvenue sur le site de Stravaganza !</h3>
<div class="text-justify flow-text">
    <p>
        <span style="margin-left:30px">Imagine une trentaine d'utcéens se réunissant pour partager leur passion pour la musique ! Ça, c'est Stravaganza.
    </p>
    <p>
        <span style="margin-left:30px">Chaque semaine les membres de notre orchestre, de notre famille, se retrouvent pour donner vie aux plus belles compositions, spécialement arrangées et dirigées par et pour des étudiants. Évidemment, et parce que le partage est une notion clé de la musique, Stravaganza organise et participe à de nombreux événements, allant de Festupic au traditionnel concert de Noël !
    </p>
    <p>
        <span style="margin-left:30px">Alors si toi aussi, tu veux rendre hommage aux grands compositeurs classiques, voyager en interprétant des musiques du monde entier, ou te replonger dans l'ambiance des films aux bandes originales devenues cultes, amène tes idées, ton expérience, sans oubliez ton instrument et ta bonne humeur, et rejoins nous !
    </p>
</div>
<h3 class="text-center">Nous rejoindre</h3>
<div class="text-justify flow-text">
    <p>
        <span style="margin-left:30px">Pour nous rejoindre, tu peux venir nous voir à la journée des associations, nous t'y attendrons ! Sinon tu peux nous contacter via <a href="mailto:stravaganza@assos.utc.fr">mail</a> et on te donnera toutes les informations !
    </p>
</div>
<div class="container center">
    <div class="embed-responsive embed-responsive-16by9" style="border:solid; border-color:#3165a4; border-width:4px; background-color: black;">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/i8bcY_SycKo?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
<br/>
<br/>