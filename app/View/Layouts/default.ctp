<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
        <title>Stravaganza - <?= $this->fetch('title');?></title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <?= $this->element('css') ?>
        <?= $this->fetch('css');?>
    </head>
    <body class="grey darken-4">
    <?= $this->element('menu') ?>
    <?php
    if (strtolower($this->params['controller']) == 'home' && strtolower($this->params['action']) == 'index') {
        echo "<div class='center red darken-3'><br/>";
        echo $this->Html->image('Logo_Strava.png', array('alt' => 'Logo', 'class' => 'img-responsive', 'style' => 'max-width: 60%;'));
        echo "<br/></div>";
    }
        ?>
    <main class="container white-text">
            <?= $this->fetch('content');?>
    </main>
    <footer class="page-footer red darken-3">
            <div class="footer-copyright">
                <div class="container center">
                    &copy;<?= date("Y")?> - Made by <a class="white-text" href="https://www.linkedin.com/in/luc-varoqui-897689125?trk=hp-identity-name">Luc</a>
                </div>
            </div>
    </footer>
    <?= $this->element('scripts') ?>
    <?= $this->fetch('script');?>
    </body>
</html>
