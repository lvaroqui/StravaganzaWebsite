<h1 class="text-center">Ajouter une partition à <strong><?= $song_name?></strong></h1>

<?= $this->Html->link('Retour', '/Partitions/ListOfPartitions/' . $song_id, array('class' => 'btn grey'))?>
<br/><br/>
<?= $this->Form->create('Partition', array('type' => 'file'));?>
<input type="hidden" name="song_id" value="<?= $song_id?>">

<div class="row">
    <div class="input-field col s12 m6">
        <select id="instrument" name="instrument_id">
            <option value="auto" selected>Automatique</option>
            <option value="conduct">Conducteur</option>
            <?php foreach($instruments as $instrument): ?>
                <option value="<?= $instrument['Instrument']['id']?>"><?=$instrument['Instrument']['name']?></option>
            <?php endforeach; ?>
        </select>
        <label for="instrument">Type</label>
    </div>

    <div class="input-field col s12 m6">
        <select id="tag" name="tag">
            <option value="null" selected>Aucun</option>
            <?php for($i = 1; $i<=4; $i++): ?>
                <option value="<?=$i?>">Partition n°<?=$i?></option>
            <?php endfor; ?>
        </select>
        <label for="tag">Tag</label>
    </div>
</div>

<div class="file-field input-field">
    <div class="btn grey waves-effect waves-light">
        <span>Partitions</span>
        <input type="file" multiple name="data[Partition][part_file][]">
    </div>
    <div class="file-path-wrapper">
        <input class="file-path validate" type="text" placeholder="Partitions (au format pdf)">
    </div>
</div>
<?= $this->Form->end(array('label' => 'Valider', 'class' => 'btn orange'));?>