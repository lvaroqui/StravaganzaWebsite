<h1 class="text-center">Ajouter un morceau</h1>

<?= $this->Html->link('Retour', '/Partitions/', array('class' => 'waves-effect waves-light btn grey'))?>
<br/><br/>
<?= $this->Form->create("Song");?>

<div class="input-field">
    <input type="text" id="name" name="name">
    <label for="name">Nom du morceau</label>
</div>
<div class="input-field">
    <input type="text" id="composer" name="composer">
    <label for="composer">Compositeur</label>
</div>
<div class="input-field">
    <input type="text" id="arranger" name="arranger">
    <label for="arranger">Arrangeur</label>
</div>

<?= $this->Form->end(array('label' => 'Valider', 'class' => 'waves-effect waves-light btn orange'));?>