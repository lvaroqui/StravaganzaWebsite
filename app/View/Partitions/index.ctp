<h1 class="text-center">Choisissez un morceau</h1>
    <div class="row">
        <div class="col s12 m12 l2 push-bot-10"><?= $this->Html->link('Retour', '/Users/Profile/', array('class' => 'waves-effect waves-light btn grey'))?></div>
    <?php if($authUser['role'] === 'admin'):?>
        <div class="col s12 m12 l7 push-bot-10">
            <?= $this->Html->link('Ajouter un morceau', '/Partitions/AddSong/', array('class' => 'right indent waves-effect waves-light btn orange'))?>
        </div>
        <div class="col s12 m12 l3" >
            <?= $this->Html->link('Impressions demandées', '/Partitions/PrintSummary/', array('class' => 'right waves-effect waves-light btn orange'))?>
        </div>
    <?php endif;?>
    </div>

    <table class="responsive-table">
        <thead>
        <tr>
            <th></th>
            <th>Nom</th>
            <th>Compositeur</th>
            <th>Stravaganzeur</th>
            <th>Ajouté le</th>
        </tr>
        </thead>
        <tbody class="white-text">
        <?php foreach($songs as $song):?>
        <tr>
            <td><?= $this->Html->link('Afficher', '/Partitions/ListOfPartitions/' . $song['Song']['id'], array('class' => 'waves-effect waves-light btn light-blue'))?></td>
            <td><?= $song['Song']['name']?></td>
            <td><?= $song['Song']['composer']?></td>
            <td><?= $song['Song']['arranger']?></td>
            <td><?=  strftime('%d %B %Y', strtotime($song['Song']['created']))?></td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>

<div id="snackbar"></div>