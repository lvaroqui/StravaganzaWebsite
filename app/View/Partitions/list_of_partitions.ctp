<h1 class="text-center"><?= $song_name?></h1>
<div class="row">
    <div class="col s12 m6 ">
        <?= $this->Html->link('Retour', '/Partitions/', array('class' => 'btn waves-effect waves-light grey push-bot-10'))?>
    </div>
    <div class="col s12 m6">
        <?php if($authUser['role'] === 'admin'):?>
            <?= $this->Html->link('Ajouter une partition', '/Partitions/AddPartition/' . $song_id, array('class' => 'btn waves-effect waves-light orange  right'))?>
        <?php endif;?>
    </div>
</div>
<h3 class="indent">Demande d'impression</h3>
<p>
    <span class="btn waves-effect waves-light green">
        <i class="material-icons">print</i>
    </span>
    pour effectuer une requête d'impression.
</p>
<p>
    <span class="btn waves-effect waves-light orange">
        <i class="material-icons">cancel</i>
    </span>
    pour annuler une requête.
</p>
<table class="">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th>Nom</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($partitions as $partition):?>
            <tr>
                <td>
                    <?= $this->Html->link('Télécharger', '/Partitions/DownloadPartition/' . $partition['id'], array('class' => 'waves-effect waves-light btn light-blue full-width'))?>
                </td>
                <td>
                    <?php if ($partition['isRequested']):?>
                        <div class="btn waves-effect waves-light orange cancelPrint" id="<?= $partition['id']?>">
                            <i class="material-icons">cancel</i>
                        </div>
                    <?php else:?>
                        <div class="btn waves-effect waves-light green askPrint" id="<?= $partition['id']?>">
                            <i class="material-icons">print</i>
                        </div>
                    <?php endif;?>
                </td>
                <td>
                    <?= $partition['name']?>
                </td>
                <td>
                    <?php if($authUser['role'] === 'admin'):?>
                        <div class="btn waves-effect waves-light red right deletePartition" id="<?= $partition['id']?>">
                            <i class="material-icons">delete</i>
                        </div>
                    <?php endif;?>
                </td>
            </tr>
    <?php endforeach;?>
    </tbody>
</table>
<div id="snackbar"></div>