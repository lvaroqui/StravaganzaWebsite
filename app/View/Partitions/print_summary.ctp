<h1 class="text-center">Partitions à imprimer</h1>
<p>
    <?= $this->Html->link('Retour', '/Partitions/', array('class' => 'btn waves-effect waves-light grey'))?>
</p>

<?php foreach($songs as $song):?>
        <h3 class="indent"><?= $song[0]["song_name"]?></h3>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Partie</th>
                    <th>Demandeur</th>
                </tr>
                </thead>
                <tbody class="white-text">
                <?php foreach($song as $claim):?>

                        <tr>
                            <td><?= $this->Html->link('Télécharger', '/Partitions/DownloadPartition/' . $claim['idPart'], array('class' => 'btn waves-effect waves-light light-blue'))?></td>
                            <td><?= $this->Html->link("Valider l'impression", '/Partitions/ValidatePrint/' . $claim['idClaim'], array('class' => 'btn waves-effect waves-light green'))?></td>
                            <td><?=$claim["name"]?></td>
                            <td><?=$claim["askedBy"]?></td>
                        </tr>

                <?php endforeach;?>
                </tbody>
        </table>
<?php endforeach;?>

        <h3 class="indent">Demandes validées</h3>
        <table class="table table-responsive">
            <thead>
            <tr>
                <th></th>
                <th>Morceau</th>
                <th>Partie</th>
                <th>Demandeur</th>
            </tr>
            </thead>
            <tbody class="white-text">
            <?php foreach($validatedClaims as $claim):?>
                <tr>

                    <td><?= $this->Html->link("Annuler la validation", '/Partitions/UnvalidatePrint/' . $claim['idClaim'], array('class' => 'btn waves-effect waves-light orange'))?></td>
                    <td><?= $claim["song_name"]?></td>
                    <td><?=$claim["name"]?></td>
                    <td><?=$claim["askedBy"]?></td>
                </tr>

            <?php endforeach;?>
            </tbody>
        </table>


<div id="snackbar"></div>
