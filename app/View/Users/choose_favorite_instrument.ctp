<div class="text-center">
    <h1>Choisi ton instrument principal</h1>
</div>
<div>
<?= $this->Form->create();?>
    <div class="input-field white-text">
        <select name="main_instrument">
        <?php foreach($instruments as $instrument):?>
            <?php $selected = ''; if ($instrument['InstrumentUser']['isMain']){$selected = 'selected';}?>
            <option value="<?= $instrument['Instrument']['id']?>" <?=$selected?>><?= $instrument['Instrument']['name']?></option>
        <?php endforeach; ?>
        </select>
    </div>
<?= $this->Form->End(array('label' => 'Valider', 'class' => 'btn orange')) ?>

</div>
