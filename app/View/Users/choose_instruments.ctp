<?php
    $checked ='';
    $row = 0;
    $this->assign('title', 'Choisissez un instrument');
?>
<div class="center-align">
    <h3 class="center">De quel(s) instrument(s) joues-tu ?</h3>
    <?= $this->Form->create();?>
    <?php foreach($categories as $category): ?>

        <?php $row = $row + 1; if($row == 1):?>
            <div class="row">
        <?php endif; ?>
        <div class="col s12 m6">
            <h4><?= $category['InstruCategory']['name']?></h4>
        <?php foreach($category['Instrument'] as $instrument): if (gettype(array_search($instrument['id'], $instruUser)) == 'integer'){$checked = 'checked';} ?>
        <p>
            <label>
                <input name="data[User][Instrument][]" id="<?= $instrument['id']?>" type="checkbox" class="filled-in checkbox-orange" value="<?= $instrument['id']?>" <?= $checked?>/>
                <span><?= $instrument['name']?></span>
            </label>
        </p>
            <?php $checked ='';?>

        <?php endforeach; ?>
        </div>
        <?php if($row == 2):?>
            </div>
        <?php $row = 0; endif;?>
    <?php endforeach; ?>
    <?php if($row == 1):?>
        </div>
    <?php endif;?>
    <div class="center">
        <?= $this->Form->End(array('label' => 'Valider', 'class' => 'btn orange')) ?>
    </div><br/>
</div>