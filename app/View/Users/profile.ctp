<?php
$this->assign('title', 'Mon profil');
?>

    <br><h2 class="center">Votre profil</h2>
<h3 class="indent">Bienvenue sur votre profil <?= $authUser['first_name'];?></h3>
<div class="row">
    <div class="col s12 m6">
    <p><?= $this->Html->link('Gestionnaire de partitions', '/Partitions', array('class' => 'btn light-blue darken-2'))?></p>
    </div>
</div>
<h3 class="indent">Vos instruments</h3>
<div class="row">
    <div class="col s6">
    <p>Instrument(s) joué(s) :</p>
        <ul>
            <?php foreach($currentInstruments as $intrument):?>
                <li><?= $intrument['Instrument']['name']?></li>
            <?php endforeach;?>
        </ul>
    <?= $this->Html->link('Choisir', array('controller' => 'Users', 'action' => 'ChooseInstruments'), array('class' => 'btn light-blue darken-2'))?>
    </div>
    <div class="col s6">
        <?php if (isset($currentMainInstrument)):?>
            <p>Instrument principal : <span class="bold" style="margin-right: 10px;: "><?= $currentMainInstrument?></span></p>
            <?= $this->Html->link('Changer', array('controller' => 'Users', 'action' => 'ChooseFavoriteInstrument'), array('class' => 'btn light-blue darken-2'))?>
        <?php endif;?>
    </div>
</div>
<?php
    if ($authUser['role'] === 'admin') {
        echo '<br><p>' . $this->Html->link("Page d'administration", '/Admins/Index', array('class' => 'btn orange')) . '</p>';
    }
?>