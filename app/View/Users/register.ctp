<div class="center">
    <h1>Inscription</h1>
<p>C'est ta première connexion au site de Stravaganza ! Finalise ton insription !
</p><br>
</div>

    <div>
        <?= $this->Form->create();?>
            <h4 class="center">Informations récupérées depuis le CAS</h4>
            <div class="row">
                <div>
                    <div class="input-field">
                        <?= $this->Form->input('Login', array('disabled', 'value' => $username))?>
                    </div>
                    <div class="input-field">
                        <?= $this->Form->input('Email', array('disabled', 'value' => $email))?>
                    </div>
                </div>
                <div class="">
                    <div class="input-field">
                        <?= $this->Form->input('firstname', array('disabled', 'value' => $firstname, 'label'=>'Prénom', 'class' => 'form-control'))?>
                    </div>
                    <div class="input-field">
                        <?= $this->Form->input('lastname', array('disabled', 'value' => $lastname, 'label'=>'Nom' , 'class' => 'form-control'))?>
                    </div>
                </div>
            </div>
        <div class="center">
            <?= $this->Form->End(array('label' => 'Valider', 'class' => 'btn light-blue')) ?>
        </div>
        <br/>
    </div>
