/**
 * Created by luc on 21/12/2016.
 */

/**
 * Created by luc on 21/12/2016.
 */$(document).ready(function () {

    $('select').formSelect();

    $('.sidenav').sidenav();

    $('.roleSelect').change(function (e) {
        e.preventDefault();
        var selected = $(this);
        selected.prop('disabled', 'disabled');

        $.ajax('ChangeRole/' + selected.val(), {
            success: function() {
                M.Toast.dismissAll();
                M.Toast({html : 'Role changed to : ' + selected.find('option:selected').text(), classes: "green"});
                selected.prop('disabled', false);
            },
            error: function (jqXHR) {
                //$.toaster({ priority : 'danger', title : 'Opération échouée', message : jqXHR.responseText});
                location.reload();
            }
        });
    });

    $('.deletePhotoGallery').click(function (e) {
        e.preventDefault();
        var clicked = $(this);
        $.ajax('DeleteImage/' + clicked.attr('id'), {
            success: function() {
                M.Toast.dismissAll();
                M.toast({html: 'Opération réussie', classes: "green"});
                clicked.parent().parent().parent().remove();
            },
            error: function (jqXHR) {
                //$.toaster({ priority : 'danger', title : 'Opération échouée', message : jqXHR.responseText});
                location.reload();
            }
        });
    });

    $('.deletePartition').click(function (e) {
        e.preventDefault();
        var clicked = $(this);
        $.ajax('../DeletePartition/' + clicked.attr('id'), {
            success: function() {
                M.Toast.dismissAll();
                M.toast({html: 'Opération réussie : partition supprimée', classes: "green"});
                clicked.parent().parent().remove();
            },
            error: function (jqXHR) {
                //$.toaster({ priority : 'danger', title : 'Opération échouée', message : jqXHR.responseText});
                location.reload();
            }
        });
    });

    $('.askPrint, .cancelPrint').click(function (e) {
        e.preventDefault();
        var clicked = $(this);
        clicked.addClass('disabled');
        if (clicked.context.classList.contains('askPrint')) {
            $.ajax('../AskPrint/' + clicked.attr('id'), {
                success: function () {
                    M.Toast.dismissAll();
                    M.toast({html: "Requête d'impression effectuée", classes: "green"});
                    clicked.removeClass('green askPrint').addClass('orange cancelPrint');
                    clicked.find($('i')).html("cancel");
                    clicked.removeClass('disabled');
                },
                error: function (jqXHR) {
                    //$.toaster({priority: 'danger', title: 'Opération échouée', message: jqXHR.responseText});
                    location.reload();
                }
            });
        }
        else {
            $.ajax('../CancelPrint/' + clicked.attr('id'), {
                success: function () {
                    M.Toast.dismissAll();
                    M.toast({html: "Requête d'impression annulée", classes: "red"});
                    clicked.addClass('green askPrint').removeClass('orange cancelPrint');
                    clicked.find($('i')).html("print");
                    clicked.removeClass('disabled');
                },
                error: function (jqXHR) {
                    //$.toaster({priority: 'danger', title: 'Opération échouée', message: jqXHR.responseText});
                    location.reload();
                }
            });
        }

    });

    $('.deleteConcert').click(function (e) {
        e.preventDefault();
        var clicked = $(this);
        console.log(clicked);
        $.ajax('../Concerts/DeleteConcert/' + clicked.attr('id'), {
            success: function() {
                M.Toast.dismissAll();
                M.Toast({html: 'Opération réussie : concert supprimée !'});
                clicked.parent().parent().remove();
            },
            error: function (jqXHR) {
                //$.toaster({ priority : 'danger', title : 'Opération échouée', message : jqXHR.responseText});
                location.reload();
            }
        });
    });
});