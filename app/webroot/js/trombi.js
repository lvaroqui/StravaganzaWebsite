/**
 * Created by luc on 01/12/2016.
 */
$(document).ready(function () {
    $(document).on('click', 'a.controls', function () {
        var index = $(this).attr('href');
        var src = $('ul.row li:nth-child(' + index + ') img').attr('src');
        $('.modal-body img').attr('src', src);
        var newPrevIndex = parseInt(index) - 1;
        var newNextIndex = parseInt(newPrevIndex) + 2;

        if ($(this).hasClass('previous')) {
            $(this).attr('href', newPrevIndex);
            $('a.next').attr('href', newNextIndex);
        } else {
            $(this).attr('href', newNextIndex);
            $('a.previous').attr('href', newPrevIndex);
        }
        var total = $('ul.row li').length + 1;
        //hide next button
        if (total === newNextIndex) {
            $('a.next').hide();
        } else {
            $('a.next').show()
        }
        //hide previous button
        if (newPrevIndex === 0) {
            $('a.previous').hide();
        } else {
            $('a.previous').show()
        }
        return false;
    });

    function animateOver(element) {
        console.log(element);
        var tl = new TimelineLite({onReverseComplete: hide, onReverseCompleteParams: [element] });

        tl.timeScale(1);
        tl.to($("#" + element.id + "_card"),0.5, { opacity: 1 });
        tl.to($("#name_display"), 0.5, { opacity: 1 }, '-=0.5');
        tl.to($("#instrument_display"), 0.5, { opacity: 1 }, '-=0.5');
        tl.to($("#role_display"), 0.5, { opacity: 1 }, '-=0.5');
        tl.play();
        return tl;
    }

    $(".Seats").hover(over, out);
    //$(".Seats").on('touchstart', over).on('touchend', out);

    function over() {
        //check if this item has an animation
        var select = $(this);
        var name = select.attr('name');
        var instrument = select.attr("data-instrument");
        var role = select.attr("data-role");

        if (name != '') {
            $("#name_display").html(name);
            $("#instrument_display").html(instrument);
            $("#role_display").html(role);

            $("#" + this.id + "_card").show();
            if (!this.animation) {
                //if not, create one
                this.animation = animateOver(this);
            } else {
                //or else play it
                this.animation.play().timeScale(1);
            }
        }
    }

    function out(element) {
        //reverse animation 4 times normal speed
        console.log("Hidding : " + this);
        this.animation.reverse().timeScale(4);
    }


    function hide(element) {
        $("#" + element.id + "_card").hide();
    }
});